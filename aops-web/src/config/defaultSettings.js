export default {
  // 主机域-主机列表更新频率30s
  domainStatusRefreshInterval: 30000,
  faultDiagnosisPropressInterval: 5000
}
